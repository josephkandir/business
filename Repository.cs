﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly DbContext context;
        public Repository(DbContext context)
        {
            this.context = context;
        }

        public int Add(T entity)
        {
            context.Add(entity);
            var result = context.SaveChanges();
            return result;
        }

        public bool Delete(int id)
        {
            var entity = Get(id);
            context.Remove(entity);
            context.SaveChanges();
            return true;
        }

        public bool Delete(string id)
        {
            var entity = Get(id);
            context.Remove(entity);
            context.SaveChanges();
            return true;
        }

        public T Get(int id)
        {
            return context.Set<T>().Find(id);
        }

        public T Get(string id)
        {
            return context.Set<T>().Find(id);
        }

        public IEnumerable<T> GetAll()
        {
            return context.Set<T>().ToList();
        }

        public IQueryable<T> GetAll(string[] entities)
        {
            IQueryable<T> query = GetAll().AsQueryable();

            if (entities != null)
                query = entities.Aggregate(query, (current, entity) => current.Include(entity));

            return query;
        }

        public void Update(T entity)
        {
            context.Entry<T>(entity).State = EntityState.Modified;
            context.SaveChanges();
        }
    }
}
