# Documentation



## Getting Started
To start usingn this repository, you first need to add the project to youe existing where you wanted to apply

## For Example
You have a model by name Contact, so, you will have an interface by name IContactRepo, and class by name ContactRepo
### Packages Used
|Package Name|Version|
|------------|-------|
|Microsoft.NETCore|3.1.0|
|Microsoft.EntityFrameworkCore|3.1.22|


### 1. Entity Interface
```
using Business;
using YourProject.Models;
using System.Collections.Generic;

namespace YourProject.Repositories
{
    public interface IContactRepo : IRepository<Contact>
    {
    }
}
```
I you want to add custom methods or function other than IRepository (default methodds/functions), then you can add in this interface particulary relating to it.
Implement the added methods or functions to ContactRepo by inheriting the interface IContactRepo followed by comma(,).

### 2. Entity Repository Class
```
using Business;
using Microsoft.EntityFrameworkCore;
using YourProject.Data;
using YourProject.Models;
using System.Collections.Generic;

namespace YourProject.Repositories
{
    public class ContactRepo : Repository<Entity>
    {
        private readonly ApplicationDbContext context;
        public ContactRepo(ApplicationDbContext context) : base(context)
        {
            this.context = context;
        }
    }
}
```

### 3. Dependency Injection
In startup class under ConfigureServices, you will add
```
services.AddScoped<IContactRepo, ContactRepo>();
```


## IRepository Explain
T here denotes the Generic DataType, which on implementation will be the model or entity
Please refer the comments of each methods for your understanding.
```
using System.Collections.Generic;
using System.Linq;

namespace Business
{
    public interface IRepository<T>
    {
        // For Add
        int Add(T entity);
        
        // For Update
        void Update(T entity);
        
        // For Delete with INT DataType
        bool Delete(int id);

        // For Delete with STRING DataType
        bool Delete(string id);
        
        // For List
        IEnumerable<T> GetAll();

        // For List with .include of other multiple entities
        IQueryable<T> GetAll(string[] entities);

        // For Get witj INT DataType
        T Get(int id);

        // For Get witj STRING DataType
        T Get(string id);

    }
}
```
