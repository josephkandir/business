﻿using System.Collections.Generic;
using System.Linq;

namespace Business
{
    public interface IRepository<T>
    {
        int Add(T entity);
        
        void Update(T entity);
        
        bool Delete(int id);
        bool Delete(string id);
        
        IEnumerable<T> GetAll();

        IQueryable<T> GetAll(string[] entities);

        T Get(int id);
        T Get(string id);

    }
}
